var swiper = new Swiper('.qomek__swiper', {
  slidesPerView: 3,
  direction: 'vertical',
  grid: {
    rows: 3
  },
  speed: 10000,
  freeMode: true,
  // autoplay: {
  //   delay: 0,
  //   disableOnInteraction: false,
  // },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
})
